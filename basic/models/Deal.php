<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property string $leadId
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
       

	$rules = []; 
		$stringItems = [['leadId','name'], 'string'];
		$integerItems  = ['id', 'amount'];		
		if (\Yii::$app->user->can('creatDeal')) {
			//$integerItems[] = 'owner';
		}
		$integerRule = [];
		$integerRule[] = $integerItems;
		$integerRule[] = 'integer';
		$ShortStringItems =[['leadId', 'name'], 'string', 'max' => 255]; 
		$rules[] = $stringItems;
		$rules[] = $integerRule;
		$rules[] = $ShortStringItems;		
		return $rules;


 /*return [
            [['id', 'leadId', 'name', 'amount'], 'required'],
            [['id', 'amount'], 'integer'],
            [['leadId', 'name'], 'string', 'max' => 255],
        ];
    }*/
	
}
	
	public function getUserole()
    {
		$roleArray = Yii::$app->authManager->
					getRolesByUser($this->id);
		$role = array_keys($roleArray)[0];				
		return	$role;
    }
	
	public function getLeadItem()
    {
        return $this->hasOne(Lead::className(), ['id' => 'name']);
    } 

	public static function getUsers()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'fullname');
		return $users;						
	}
	
	public static function getUsersWithAllUsers()
	{
		$users = self::getUsers();
		$users[-1] = 'All Users';
		$users = array_reverse ( $users, true );
		return $users;	
	}	
	
	
	public static function getRoles()
	{
		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		return $roles; 	
	}

	public static function getRolesWithAllRoles()
	{
		$roles = self::getRoles();
		$roles[-1] = 'All Roles';
		$roles = array_reverse ( $roles, true );
		return $roles;	
	}	
	
	  public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

       /* if ($this->isNewRecord)
		    $this->auth_key = Yii::$app->security->generateRandomString(32);*/

        return $return;
    }
	
	public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);

        if (!\Yii::$app->user->can('creatDeal')){
			return $return;
		}
		$auth = Yii::$app->authManager;
		//$roleName = $this->role; 
		$role = $auth->getRole($roleName);
		//if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
			//$auth->assign($role, $this->id);
		//} else {
		//	$db = \Yii::$app->db;
			//$db->createCommand()->delete('auth_assignment',
			//	['deal_id' => $this->id])->execute();
		//	$auth->assign($role, $this->id);
	//	}

        return $return;
    }		
	



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
}
