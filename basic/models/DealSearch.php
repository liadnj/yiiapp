<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Deal;

/**
 * DealSearch represents the model behind the search form about `app\models\Deal`.
 */
class DealSearch extends Deal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'amount'], 'integer'],
            [['leadId', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
     
        $query = Deal::find();

        // add conditions that should always apply here
	$this->load($params);

	
		
		
		 $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
		

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'amount' => $this->amount,
			/*'leadId' => $this->leadId,
				'name' => $this->name,*/
        ]);

        $query->andFilterWhere(['like', 'leadId', $this->leadId])
            ->andFilterWhere(['like', 'name', $this->name]);
			  
			  $query->andFilterWhere(['>=','amount',$this->amount]);
          //  ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
